import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {VESDK} from 'react-native-videoeditorsdk';
const App = () => {
  const showVideoEditorExample = async () => {
    const configuration = {
      composition: {
        // By default the user is allowed to add personal video clips
        // from the device. For this example the user is only allowed
        // to add video clips that are predefined in the editor configuration.
        personalVideoClips: false,
      },
    };
    try {
      // Add a video from the assets directory.
      const video = require('./assets/sample.mp4');
      // Open the video editor and handle the export as well as any occuring errors.
      const result = await VESDK.openEditor(video, configuration);
      if (result != null) {
        // The user exported a new video successfully and the newly generated video is located at `result.video`.
        console.log(result?.video, 'success video');
      } else {
        // The user tapped on the cancel button within the editor.
        return;
      }
    } catch (error) {
      // There was an error generating the video.
      console.log(error, 'cancel video');
    }
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={showVideoEditorExample}
        style={{
          width: 100,
          height: 45,
          backgroundColor: 'pink',
          borderRadius: 12,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text>click</Text>
      </TouchableOpacity>
    </View>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2c3e50',
  },
});

//make this component available to the app
export default App;
